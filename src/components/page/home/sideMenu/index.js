import React, { Component } from 'react'
import { Menu, Icon, Switch } from 'antd';
import './index.scss'
class SideMenu extends Component{
    constructor(props){
        super(props);
        this.state = {
            editMode: false
        };
        this.renderMenuData = this.renderMenuData.bind(this);
        this.onEditModeChange = this.onEditModeChange.bind(this);
        this.onDelClick = this.onDelClick.bind(this);

    }
    renderMenuData(data){
        if(!data){
            return null;
        }
        return data.map(item=>{
            if(item.childern){
                return (
                    <Menu.SubMenu key={item.menuId} title={<span className='menuTitle'><Icon type={item.icon} /><span>{item.title} {this.state.editMode ? <Icon type="minus-circle" className='delBtn' onClick={this.onDelClick.bind(this,item.menuId)}/>: '' }</span></span>}>
                        {this.renderMenuData(item.childern)}
                    </Menu.SubMenu>
                )
            }else{
                return (
                    <Menu.Item key={item.menuId} ><span className='menuTitle'>{item.title} {this.state.editMode ? <Icon type="minus-circle" className='delBtn' onClick={this.onDelClick.bind(this,item.menuId)}/>: '' }</span></Menu.Item>
                )
            }
        })
    }
    onEditModeChange(editMode){
        this.setState({editMode});
    }
    onDelClick(id,e){
        e.stopPropagation();
        console.log(id);
        let result = id.split("-");
        console.log(result);
        let data;
        if(result.length === 1){
            data = this.props.menuData.filter(item => {
                return item.menuId === result[0] ? false : true;
            })
            console.log(data);
        }else{
            data = this.props.menuData.map(item => {
                if(item.menuId === result[0]){
                    item.childern = item.childern.filter(item => {
                        return item.menuId === id ? false : true;
                    })
                }
                return item;
            })
            console.log(data);
        }

    }
    render(){
        return (
            <div className='menuWrap'>
                <div className="logo"></div>
                <Menu theme="dark" mode="inline" defaultSelectedKeys={['1']} className='menu'>
                    {this.renderMenuData(this.props.menuData)}
                </Menu>
                {
                    this.props.collapsed ? '' : (
                        <div className='editMenu' >
                            <span >编辑模式</span>
                            <Switch checkedChildren="开" unCheckedChildren="关" defaultChecked={false} onChange={this.onEditModeChange}></Switch>
                            <span className='contralBtn'>
                                <Icon type="reload" className='btn'/>
                                <Icon type="save" className='btn'/>
                            </span>

                        </div>
                    )
                }

            </div>

        )
    }
}
export default SideMenu;
SideMenu.defaultProps = {
    menuData : [
        {'menuId' : '0001', icon: 'mail', 'title' : 'Navigation One', childern: [
                {'menuId' : '0001-111', 'title' : 'Option 1'},
                {'menuId' : '0001-222', 'title' : 'Option 2'},
                {'menuId' : '0001-333', 'title' : 'Option 3'},
                {'menuId' : '0001-444', 'title' : 'Option 4'}
            ]},
        {'menuId' : '0002', icon: 'mail', 'title' : 'Navigation One', childern: [
                {'menuId' : '0002-111', 'title' : 'Option 7'},
                {'menuId' : '0002-222', 'title' : 'Option 8'}


            ]},
    ]
}