import React, { Component } from 'react';
import './contentCard.scss';
import $ from 'jquery';
import { Icon } from 'antd'
class ContentCard extends Component {
    constructor(props){
        super(props);
        this.state = {
            contentCardId: props.contentCardInfo.contentCardId,
            height: props.contentCardInfo.height,
            width: props.contentCardInfo.width,
            positionX: props.contentCardInfo.positionX,
            positionY: props.contentCardInfo.positionY,

        }
    }
    componentDidMount() {
        this.scrollTop = 0;
        this.scrollLeft = 0;
        $('#'+this.state.contentCardId).mousedown((e) => {
            e.preventDefault()
            $('#'+this.state.contentCardId).css({zIndex: this.props.zIndex + 1});
            this.props.changeZIndex();
        })
        $('#'+this.state.contentCardId).find(".contentTitle").mousedown((e) =>{
            e.preventDefault()
            let distenceX = e.pageX - $('#'+this.state.contentCardId).offset().left;
            let distenceY = e.pageY - $('#'+this.state.contentCardId).offset().top;
            $('.contentAreaWrap').mousemove((e) => {
                let x =  e.pageX - $('.contentAreaWrap').offset().left + this.scrollLeft  - distenceX ;
                let y =  e.pageY - $('.contentAreaWrap').offset().top + this.scrollTop - distenceY ;
                if(x< 0){
                    x = 0;
                }
                if( y < 50 ){
                    y = 50;
                }
                $('#'+this.state.contentCardId).css({top: y, left: x})
            });
            $(document).mouseup(() =>{
                $('.contentAreaWrap').off('mousemove');
            });
        });
        $('.contentAreaWrap').scroll((e)=>{
            this.scrollTop = $('.contentAreaWrap').scrollTop();
            this.scrollLeft = $('.contentAreaWrap').scrollLeft();
        })
        $('#'+this.state.contentCardId).find('.reSizeSE').mousedown((e)=>{
            e.preventDefault();
            let startX = e.pageX;
            let startY = e.pageY;
            let startW = $('#'+this.state.contentCardId).innerWidth();
            let startH = $('#'+this.state.contentCardId).innerHeight();
            $('.contentAreaWrap').mousemove((e) => {
                let x =  e.pageX -  startX + startW
                let y =  e.pageY - startY + startH;
                if(x < 200){
                    x = 200;
                }
                if(y < 200){
                    y = 200;
                }
                $('#'+this.state.contentCardId).css({height: y, width: x})
            });
            $(document).mouseup(() =>{
                $('.contentAreaWrap').off('mousemove');
            });
        })
        $('#'+this.state.contentCardId).find('.reSizeSW').mousedown((e)=>{
            e.preventDefault();
            let startX = e.pageX;
            let startY = e.pageY;
            let startW = $('#'+this.state.contentCardId).innerWidth();
            let startH = $('#'+this.state.contentCardId).innerHeight();
            let startLeft = $('#'+this.state.contentCardId).position().left;
            $('.contentAreaWrap').mousemove((e) => {
                let x =  -(e.pageX -  startX) + startW
                let y =  e.pageY - startY + startH;
                if(x < 200){
                    x = 200;
                }
                if(y < 200){
                    y = 200;
                }
                $('#'+this.state.contentCardId).css({height: y, width: x })
                x > 200 ? $('#'+this.state.contentCardId).css({left:  startLeft + (e.pageX -  startX)}) : ''
            });
            $(document).mouseup(() =>{
                $('.contentAreaWrap').off('mousemove');
            });
        })

    }
        render() {
            let {contentCardId, positionX, positionY, height, width} = this.state;
            let { isEditModel } = this.props;
            return (
                <div id={contentCardId} className={isEditModel ? "contentCard edit" : 'contentCard'}
                     style={{height: height+'px', width: width+'px', left: positionX+'px', top: positionY+'px'}} >
                    {isEditModel ? <Icon type="close-circle-o" className='delBtn'/> : ""}
                    <div className='contentTitle'>
                        <span>Title</span><Icon type="setting" className='settingBtn'/>
                    </div>
                    <div className='content'>
                        Content
                    </div>
                    <div className="reSizeSE">
                    </div>
                    <div className="reSizeSW">
                    </div>
                </div>
            );
        }
}
export default ContentCard;