import React, {Component} from 'react';
import './index.scss'
import { Icon } from 'antd';
import ContentCard from '../contentCard'
import $ from 'jquery'
class ContentArea extends Component{
    constructor(props) {
        super(props);

        this.state = {
            zIndex: 0,
            isEditModel: false,
            contentCards: [
                {contentCardId: '10000', height: 200, width: 200, positionX: 300, positionY: 300, status: 0},
                {contentCardId: '10001', height: 200, width: 200, positionX: 300, positionY: 600, status: 0},
            ]
        }

    }
    changeZIndex = () => {
        let zIndex = this.state.zIndex;
        zIndex ++;
        this.setState({zIndex});
    }
    onEditBtnClick = () => {
        $('.contralBar').slideDown()
        this.setState({isEditModel: true})
    }
    onSaveBtnClick = () => {
        $('.contralBar').slideUp()
        this.setState({isEditModel: false})
    }
    renderContentCard = () => {
        let { zIndex, contentCards, isEditModel } = this.state;
        return contentCards.map( item =>  (
                <ContentCard key={item.contentCardId} zIndex={zIndex} changeZIndex={this.changeZIndex} contentCardInfo={item} isEditModel={isEditModel} ></ContentCard>
            )
        )
    }
    addContentCard = () => {
        let { contentCards } = this.state;
        contentCards = [...contentCards];
        let newId = parseInt( contentCards[contentCards.length - 1].contentCardId ) + 1;
        contentCards.push({contentCardId: newId+'', height: 200, width: 200, positionX: 60, positionY: 60, status: 1});
        this.setState({contentCards});
    }
    componentDidMount(){
        $('.contralBar').slideUp();
    }
    render() {
        let { zIndex, isEditModel } = this.state;
        return (
            <div className='contentAreaWrap'>
                <div className="contralBar"><Icon type="plus-square-o" className='btn' onClick={this.addContentCard}/><Icon type="reload" className='btn'/><Icon type="save" className='btn' onClick={this.onSaveBtnClick}/></div>
                <Icon type="form" className='editBtn' onClick={this.onEditBtnClick}/>
                {this.renderContentCard()}
            </div>
        )
    }

}
export default ContentArea;


