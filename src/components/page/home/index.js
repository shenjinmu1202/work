import React, { Component } from 'react'
import { Layout, Icon} from 'antd';
import SideMenu from './sideMenu';
import './index.scss';
import ContentArea from './contentArea';
const { Header, Sider, Content } = Layout;

class SiderDemo extends Component {
    state = {
        collapsed: false,
    };

    toggle = () => {
        this.setState({
            collapsed: !this.state.collapsed,
        });
    }

    render() {
        return (
            <Layout>
                <Sider
                    // trigger={null}
                    // collapsible
                    // collapsed={this.state.collapsed}
                    breakpoint="lg"
                    collapsedWidth="0"
                >
                    <div className="logo" />
                    <SideMenu collapsed={this.state.collapsed}></SideMenu>


                </Sider>
                <Layout>
                    <Header style={{ background: '#fff', padding: 0 }}>
                        <Icon
                            className="trigger"
                            type={this.state.collapsed ? 'menu-unfold' : 'menu-fold'}
                            onClick={this.toggle}
                        />
                    </Header>
                    <Content style={{ margin: '24px  0  0 16px', background: '#fff', minHeight: 280 }} className='content'>
                        <ContentArea></ContentArea>
                    </Content>
                </Layout>
            </Layout>
        );
    }
}

export default SiderDemo;